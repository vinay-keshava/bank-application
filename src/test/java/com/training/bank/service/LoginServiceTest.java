package com.training.bank.service;



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.training.bank.dto.LoginDto;
import com.training.bank.dto.LoginResponseDto;
import com.training.bank.entity.Account;
import com.training.bank.entity.AccountType;
import com.training.bank.entity.Customer;
import com.training.bank.entity.IfscCode;
import com.training.bank.entity.LoggedInStatus;
import com.training.bank.exception.CustomerAlreadyLoggedIn;
import com.training.bank.exception.CustomerNotFound;
import com.training.bank.exception.LoginFailedException;
import com.training.bank.repository.CustomerRepository;
import com.training.bank.service.impl.LoginServiceImpl;

@ExtendWith(MockitoExtension.class)
class LoginServiceTest {

	@Mock
	CustomerRepository customerRepository;

	@InjectMocks
	LoginServiceImpl impl;

	@Test
	void testCustomerNotFound(){
		Customer customer = null;
		LoginDto dto = new LoginDto("vinay@gmail.com","vinay");
//		dto.setEmail("vinay@gmail.com");
//		dto.setPassword("vinay");
		Mockito.when(customerRepository.findByEmailAndPassword(dto.email(), dto.password())).thenReturn(customer);
		assertThrows(CustomerNotFound.class, ()-> {
			impl.customerLogin(dto);
		});
	}
	
	@Test
	void testCustomerNotLoggedIn() {
		Customer customer = new Customer();
		customer.setLoggedInStatus(LoggedInStatus.LOGGEDIN);
		
		LoginDto dto = new LoginDto("vinay@gmail.com","vinay");
//		dto.setEmail("vinay@gmail.com");
//		dto.setPassword("vinay");
		Mockito.when(customerRepository.findByEmailAndPassword(dto.email(), dto.password())).thenReturn(customer);
		assertThrows(CustomerAlreadyLoggedIn.class, ()->{
			impl.customerLogin(dto);
		});
	}
	
	@Test
	void testCustomerLoginSuccessfull() {
		Customer customer = new Customer();
		customer.setAadharNumber(898989898989L);
		customer.setLoggedInStatus(LoggedInStatus.LOGGEDOUT);
		customer.setCustomerId(1L);
		customer.setEmail("vinaykeshava33@gmail.com");
		customer.setPassword("vinay");
		
		Account account = new Account();
		account.setAccoundId(1L);
		account.setAccountIfscCode(IfscCode.CNRB2024);
		account.setAccountNumber(7878787878L);
		account.setBalance(1000.00);
		account.setAccountType(AccountType.SAVINGS);
		customer.setAccount(account);
		LoginDto dto = new LoginDto("vinay@gmail.com","vinay");
		Mockito.when(customerRepository.findByEmailAndPassword(dto.email(), dto.password())).thenReturn(customer);
		
		LoginResponseDto dto2 = impl.customerLogin(dto);
		assertEquals(dto2.getAccountBalance(),customer.getAccount().getBalance());
		assertEquals(dto2.getAccountNumber(),customer.getAccount().getAccountNumber());
		assertEquals(dto2.getAccountType(), customer.getAccount().getAccountType());
	}
	
	@Test
	void testCustomerLoginFailed() {
		Customer customer = new Customer();
		customer.setAadharNumber(898989898989L);
		customer.setLoggedInStatus(LoggedInStatus.LOGGEDOUT);
		customer.setCustomerId(1L);
		customer.setEmail("vinaykeshava33@gmail.com");
		customer.setPassword("vinay");
		
		Account account = new Account();
		account.setAccoundId(1L);
		account.setAccountIfscCode(IfscCode.CNRB2024);
		account.setAccountNumber(7878787878L);
		account.setBalance(1000.00);
		account.setAccountType(AccountType.SAVINGS);
		customer.setAccount(account);
		LoginDto dto = new LoginDto("vinay@gmail.com","vinay");
//		dto.setEmail("vinay@gmail.com");
//		dto.setPassword("keshava");
		Mockito.when(customerRepository.findByEmailAndPassword(dto.email(), dto.password())).thenReturn(customer);
		
		assertThrows(LoginFailedException.class, ()->{
			impl.customerLogin(dto);
		});
	}

}
