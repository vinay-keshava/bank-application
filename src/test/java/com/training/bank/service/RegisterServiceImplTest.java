package com.training.bank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.bank.dto.RegisterDto;
import com.training.bank.dto.ResponseDto;
import com.training.bank.entity.Customer;
import com.training.bank.exception.CustomerAlreadyExists;
import com.training.bank.repository.AccountRepository;
import com.training.bank.repository.CustomerRepository;
import com.training.bank.service.impl.RegisterServiceImpl;

@ExtendWith(SpringExtension.class)
class RegisterServiceImplTest {

	@Mock
	CustomerRepository customerRepository;
	
	@Mock
	AccountRepository accountRepository;

	@InjectMocks
	RegisterServiceImpl registerServiceImpl;

	@Test
	void testRegisterCustomerSuccess() {
		RegisterDto dto = new RegisterDto();
		dto.setCustomerName("darshan");
		dto.setEmail("darshan@gail.com");
		dto.setAmount(Double.valueOf(2000));
		dto.setAadharNumber(121212122l);

		Customer customer = new Customer();

		Mockito.when(customerRepository.findByEmail(dto.getEmail())).thenReturn(Optional.empty());
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		ResponseDto response = registerServiceImpl.registerCustomer(dto);

		assertEquals("customer account created succesfully", response.getMessage());
		assertEquals(201, response.getHttpStatusCode());

	}

	@Test
	void testCustomerAlreadyExistCheck() {
		RegisterDto dto = new RegisterDto();
		dto.setCustomerName("darshan");
		dto.setEmail("darshan@gail.com");
		dto.setAmount(Double.valueOf(2000));
		dto.setAadharNumber(121212122l);

		Customer customer = new Customer();
		customer.setCustomerName("darshan");

		Mockito.when(customerRepository.findByEmail(dto.getEmail())).thenReturn(Optional.of(customer));
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);

		assertThrows(CustomerAlreadyExists.class, () -> {
			registerServiceImpl.registerCustomer(dto);
		});
	}
}
