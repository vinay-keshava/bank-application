package com.training.bank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.bank.dto.FundTransferDto;
import com.training.bank.dto.ResponseDto;
import com.training.bank.entity.Account;
import com.training.bank.entity.Beneficiary;
import com.training.bank.entity.BeneficiaryAccount;
import com.training.bank.entity.Customer;
import com.training.bank.entity.FundTransfer;
import com.training.bank.entity.TransactionType;
import com.training.bank.exception.AccountMissMatchException;
import com.training.bank.exception.AccountNotFound;
import com.training.bank.exception.CustomerNotFound;
import com.training.bank.exception.InsufficientBalanceExcpetion;
import com.training.bank.repository.AccountRepository;
import com.training.bank.repository.BeneficiaryRepository;
import com.training.bank.repository.CustomerRepository;
import com.training.bank.repository.FundTransferRepository;
import com.training.bank.service.impl.FundTransferServiceImpl;

@ExtendWith(SpringExtension.class)
class FundTransferServiceImplTest {

	@Mock
	CustomerRepository customerRepository;

	@Mock
	AccountRepository accountRepository;

	@Mock
	BeneficiaryRepository beneficiaryRepository;

	@Mock
	FundTransferRepository fundTransferRepository;

	@InjectMocks
	FundTransferServiceImpl fundTransferServiceImpl;

	@Test
	void testFundTransferSuccessForCredit() {

		Account account = new Account();
		account.setAccountNumber(12121212121l);
		account.setBalance(Double.valueOf(5000));

		Customer customer = new Customer();
		customer.setAccount(account);

		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		beneficiaryAccount.setAccountHolderName("darshan");
		beneficiaryAccount.setAccountNumber(6356256215372l);

		BeneficiaryAccount beneficiaryAccount2 = new BeneficiaryAccount();
		beneficiaryAccount2.setAccountHolderName("darshan");
		beneficiaryAccount2.setAccountNumber(34324342343l);

		List<BeneficiaryAccount> beneficiary = new ArrayList<>();
		beneficiary.add(beneficiaryAccount);
		beneficiary.add(beneficiaryAccount2);

		FundTransferDto dto = new FundTransferDto();
		dto.setTransactionAmount(Double.valueOf(500));
		dto.setAccountNumber(12121212121l);
		dto.setBeneficiaryAccountNumber(6356256215372l);
		dto.setTransactionType(TransactionType.CREDIT);

		Beneficiary beneficiarys = new Beneficiary();
		beneficiarys.setCustomer(customer);
		beneficiarys.setBeneficiaryAccount(beneficiary);

		FundTransfer fundTransfer = new FundTransfer();

		Mockito.when(customerRepository.findByCustomerId(dto.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByAccountNumber(dto.getAccountNumber())).thenReturn(account);
		Mockito.when(beneficiaryRepository.findByCustomerCustomerId(dto.getCustomerId())).thenReturn(beneficiarys);
		Mockito.when(fundTransferRepository.save(fundTransfer)).thenReturn(fundTransfer);

		ResponseDto result = fundTransferServiceImpl.fundTransfer(dto);
		assertEquals("Transaction Successfull", result.getMessage());
		assertEquals(200, result.getHttpStatusCode());

	}

	@Test
	void testFundTransferSuccessForDebit() {

		Account account = new Account();
		account.setAccountNumber(12121212121l);
		account.setBalance(Double.valueOf(5000));

		Customer customer = new Customer();
		customer.setAccount(account);

		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		beneficiaryAccount.setAccountHolderName("darshan");
		beneficiaryAccount.setAccountNumber(6356256215372l);

		BeneficiaryAccount beneficiaryAccount2 = new BeneficiaryAccount();
		beneficiaryAccount2.setAccountHolderName("darshan");
		beneficiaryAccount2.setAccountNumber(34324342343l);

		List<BeneficiaryAccount> beneficiary = new ArrayList<>();
		beneficiary.add(beneficiaryAccount);
		beneficiary.add(beneficiaryAccount2);

		FundTransferDto dto = new FundTransferDto();
		dto.setTransactionAmount(Double.valueOf(500));
		dto.setAccountNumber(12121212121l);
		dto.setBeneficiaryAccountNumber(6356256215372l);
		dto.setTransactionType(TransactionType.DEBIT);

		Beneficiary beneficiarys = new Beneficiary();
		beneficiarys.setCustomer(customer);
		beneficiarys.setBeneficiaryAccount(beneficiary);

		FundTransfer fundTransfer = new FundTransfer();

		Mockito.when(customerRepository.findByCustomerId(dto.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByAccountNumber(dto.getAccountNumber())).thenReturn(account);
		Mockito.when(beneficiaryRepository.findByCustomerCustomerId(dto.getCustomerId())).thenReturn(beneficiarys);
		Mockito.when(fundTransferRepository.save(fundTransfer)).thenReturn(fundTransfer);

		ResponseDto result = fundTransferServiceImpl.fundTransfer(dto);
		assertEquals("Transaction Successfull", result.getMessage());
		assertEquals(200, result.getHttpStatusCode());

	}

	@Test
	void testFundTransferCheckForCustmerNotFound() {
		Account account = new Account();
		account.setAccountNumber(12121212121l);
		account.setBalance(Double.valueOf(5000));

		Customer customer = new Customer();
		customer.setAccount(account);

		FundTransferDto dto = new FundTransferDto();
		dto.setTransactionAmount(Double.valueOf(500));
		dto.setAccountNumber(12121212121l);
		dto.setBeneficiaryAccountNumber(6356256215372l);
		dto.setTransactionType(TransactionType.DEBIT);
		Mockito.when(customerRepository.findByCustomerId(dto.getCustomerId())).thenReturn(Optional.empty());
		assertThrows(CustomerNotFound.class, () -> {
			fundTransferServiceImpl.fundTransfer(dto);
		});

	}

	@Test
	void testFundTransferCheckForaccountNotFound() {
		Account account = new Account();
		account.setAccountNumber(12121212121l);
		account.setBalance(Double.valueOf(5000));

		Customer customer = new Customer();
		customer.setAccount(account);

		FundTransferDto dto = new FundTransferDto();
		dto.setTransactionAmount(Double.valueOf(500));
		dto.setAccountNumber(12121212121l);
		dto.setBeneficiaryAccountNumber(6356256215372l);
		dto.setTransactionType(TransactionType.DEBIT);
		Mockito.when(customerRepository.findByCustomerId(dto.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByAccountNumber(dto.getAccountNumber())).thenReturn(null);

		assertThrows(AccountNotFound.class, () -> {
			fundTransferServiceImpl.fundTransfer(dto);
		});

	}

	@Test
	void testFundTransferCheckForAccountMissMatch() {
		Account account1 = new Account();
		account1.setAccountNumber(76876878943l);
		account1.setBalance(Double.valueOf(5000));

		Account account = new Account();
		account.setAccountNumber(12121212121l);
		account.setBalance(Double.valueOf(5000));

		Customer customer = new Customer();
		customer.setAccount(account);

		FundTransferDto dto = new FundTransferDto();
		dto.setTransactionAmount(Double.valueOf(500));
		dto.setAccountNumber(765765765l);
		dto.setBeneficiaryAccountNumber(6356256215372l);
		dto.setTransactionType(TransactionType.DEBIT);
		Mockito.when(customerRepository.findByCustomerId(dto.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByAccountNumber(dto.getAccountNumber())).thenReturn(account1);

		assertThrows(AccountMissMatchException.class, () -> {
			fundTransferServiceImpl.fundTransfer(dto);
		});

	}

	@Test
	void testFundTransferForInsufficientBalance() {

		Account account = new Account();
		account.setAccountNumber(12121212121l);
		account.setBalance(Double.valueOf(500));

		Customer customer = new Customer();
		customer.setAccount(account);

		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		beneficiaryAccount.setAccountHolderName("darshan");
		beneficiaryAccount.setAccountNumber(6356256215372l);

		BeneficiaryAccount beneficiaryAccount2 = new BeneficiaryAccount();
		beneficiaryAccount2.setAccountHolderName("darshan");
		beneficiaryAccount2.setAccountNumber(34324342343l);

		List<BeneficiaryAccount> beneficiary = new ArrayList<>();
		beneficiary.add(beneficiaryAccount);
		beneficiary.add(beneficiaryAccount2);

		FundTransferDto dto = new FundTransferDto();
		dto.setTransactionAmount(Double.valueOf(5000));
		dto.setAccountNumber(12121212121l);
		dto.setBeneficiaryAccountNumber(6356256215372l);
		dto.setTransactionType(TransactionType.DEBIT);

		Beneficiary beneficiarys = new Beneficiary();
		beneficiarys.setCustomer(customer);
		beneficiarys.setBeneficiaryAccount(beneficiary);

		FundTransfer fundTransfer = new FundTransfer();

		Mockito.when(customerRepository.findByCustomerId(dto.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByAccountNumber(dto.getAccountNumber())).thenReturn(account);
		Mockito.when(beneficiaryRepository.findByCustomerCustomerId(dto.getCustomerId())).thenReturn(beneficiarys);
		Mockito.when(fundTransferRepository.save(fundTransfer)).thenReturn(fundTransfer);

		assertThrows(InsufficientBalanceExcpetion.class, () -> {
			fundTransferServiceImpl.fundTransfer(dto);
		});

	}

	@Test
	void testFundTransferForBeneficairyMissMatch() {

		Account account = new Account();
		account.setAccountNumber(12121212121l);
		account.setBalance(Double.valueOf(5000));

		Customer customer = new Customer();
		customer.setAccount(account);

		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		beneficiaryAccount.setAccountHolderName("darshan");
		beneficiaryAccount.setAccountNumber(6356256215372l);

		BeneficiaryAccount beneficiaryAccount2 = new BeneficiaryAccount();
		beneficiaryAccount2.setAccountHolderName("darshan");
		beneficiaryAccount2.setAccountNumber(34324342343l);

		List<BeneficiaryAccount> beneficiary = new ArrayList<>();
		beneficiary.add(beneficiaryAccount);
		beneficiary.add(beneficiaryAccount2);

		FundTransferDto dto = new FundTransferDto();
		dto.setTransactionAmount(Double.valueOf(500));
		dto.setAccountNumber(12121212121l);
		dto.setBeneficiaryAccountNumber(343434343l);
		dto.setTransactionType(TransactionType.DEBIT);

		Beneficiary beneficiarys = new Beneficiary();
		beneficiarys.setCustomer(customer);
		beneficiarys.setBeneficiaryAccount(beneficiary);

		FundTransfer fundTransfer = new FundTransfer();

		Mockito.when(customerRepository.findByCustomerId(dto.getCustomerId())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByAccountNumber(dto.getAccountNumber())).thenReturn(account);
		Mockito.when(beneficiaryRepository.findByCustomerCustomerId(dto.getCustomerId())).thenReturn(beneficiarys);
		Mockito.when(fundTransferRepository.save(fundTransfer)).thenReturn(fundTransfer);

		assertThrows(AccountNotFound.class, () -> {
			fundTransferServiceImpl.fundTransfer(dto);
		});

	}

}
