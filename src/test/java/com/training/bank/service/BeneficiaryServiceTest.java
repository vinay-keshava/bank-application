package com.training.bank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.training.bank.dto.BeneficiaryAccountDto;
import com.training.bank.dto.BeneficiaryDto;
import com.training.bank.entity.Account;
import com.training.bank.entity.AccountType;
import com.training.bank.entity.Beneficiary;
import com.training.bank.entity.BeneficiaryAccount;
import com.training.bank.entity.Customer;
import com.training.bank.entity.IfscCode;
import com.training.bank.entity.LoggedInStatus;
import com.training.bank.exception.CustomerNotFound;
import com.training.bank.exception.CustomerNotLoggedIn;
import com.training.bank.repository.AccountRepository;
import com.training.bank.repository.BeneficiaryRepository;
import com.training.bank.repository.CustomerRepository;
import com.training.bank.service.impl.BeneficiaryServiceImpl;

@ExtendWith(MockitoExtension.class)
class BeneficiaryServiceTest {
	
	@Mock
	CustomerRepository customerRepository;
	
	@Mock
	AccountRepository accountRepository;
	
	@Mock
	BeneficiaryRepository beneficiaryRepository;
	
	@InjectMocks
	BeneficiaryServiceImpl impl;

	@Test
	void testCustomerNotFound() {
		
		Optional<Customer> customer = null;
		BeneficiaryDto beneficiaryDto = new BeneficiaryDto();
		beneficiaryDto.setCustomer(1L);
		Mockito.when(customerRepository.findByCustomerId(beneficiaryDto.getCustomer())).thenReturn(customer);
		assertThrows(CustomerNotFound.class, ()-> {
			impl.addBenefeciary(beneficiaryDto);
		});
	}
	
	@Test
	void testCustomerNotLoggedIn() {
		
		Optional<Customer> customer = Optional.ofNullable(new Customer());
		customer.get().setLoggedInStatus(LoggedInStatus.LOGGEDOUT);
		customer.get().setCustomerId(1L);

		BeneficiaryDto beneficiaryDto = new BeneficiaryDto();
		beneficiaryDto.setCustomer(1L);
		Mockito.when(customerRepository.findByCustomerId(beneficiaryDto.getCustomer())).thenReturn(customer);
		
		assertThrows(CustomerNotLoggedIn.class, ()-> {
			impl.addBenefeciary(beneficiaryDto);
		});
	}
	
	@Test
	void testAddBeneficiary() {
		
		BeneficiaryDto beneficiaryDto = new BeneficiaryDto();
		beneficiaryDto.setCustomer(1L);
		List<BeneficiaryAccountDto> accountDtos = new ArrayList<>();
		accountDtos.add(new BeneficiaryAccountDto("mahesh", "UBIN5649", 7878787878L));
		accountDtos.add(new BeneficiaryAccountDto("suresh", "UBIN5650", 7878787879L));
		accountDtos.add(new BeneficiaryAccountDto("lokesh", "UBIN5651", 7878787880L));
		accountDtos.add(new BeneficiaryAccountDto("vikesh", "UBIN5652", 7878787881L));
		beneficiaryDto.setBeneficiaryAccounts(accountDtos);
		
		Optional<Customer> customer = Optional.ofNullable(new Customer());
		customer.get().setCustomerId(1L);
		customer.get().setLoggedInStatus(LoggedInStatus.LOGGEDIN);
		customer.get().setCustomerId(1L);
		Account account = new Account();
		account.setAccoundId(1L);
		account.setAccountIfscCode(IfscCode.CNRB2024);
		account.setAccountNumber(7878787878L);
		account.setBalance(1000.00);
		account.setAccountType(AccountType.SAVINGS);
		customer.get().setAccount(null);
		Mockito.when(customerRepository.findByCustomerId(beneficiaryDto.getCustomer())).thenReturn(customer);
		
		Beneficiary beneficiary = new Beneficiary();
		beneficiary.setCustomer(customer.get());
		
		List<BeneficiaryAccount> accounts = new ArrayList<>();
		accounts.add(new BeneficiaryAccount(1L,"mahesh", "UBIN5649", 7878787878L));
		accounts.add(new BeneficiaryAccount(2L,"suresh", "UBIN5650", 7878787879L));
		accounts.add(new BeneficiaryAccount(3L,"lokesh", "UBIN5651", 7878787880L));
		accounts.add(new BeneficiaryAccount(4L,"vikesh", "UBIN5652", 7878787881L));
		beneficiary.setBeneficiaryAccount(accounts);
		assertEquals("Beneficiary account details added successfully", impl.addBenefeciary(beneficiaryDto).getMessage());
	}
}
