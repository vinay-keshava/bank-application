package com.training.bank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import com.training.bank.dto.BeneficiaryAccountDto;
import com.training.bank.dto.BeneficiaryDto;
import com.training.bank.dto.ResponseDto;
import com.training.bank.service.impl.BeneficiaryServiceImpl;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@ExtendWith(MockitoExtension.class)
class BeneficiaryControllerTest {

	@Mock
	BeneficiaryServiceImpl beneficiaryServiceImpl;

	@InjectMocks
	BeneficiaryController beneficiaryController;

	public ResponseEntity<ResponseDto> addBeneficiaryDetails(@RequestBody BeneficiaryDto beneficiaryDto) {
		return new ResponseEntity<>(beneficiaryServiceImpl.addBenefeciary(beneficiaryDto), HttpStatus.CREATED);
	}

	@Test
	void testaddBeneficiaryDetails() {

		List<BeneficiaryAccountDto> accounts = new ArrayList<>();
		accounts.add(new BeneficiaryAccountDto("mahesh", "UBIN5649", 7878787878L));
		accounts.add(new BeneficiaryAccountDto("suresh", "UBIN5650", 7878787879L));
		accounts.add(new BeneficiaryAccountDto("lokesh", "UBIN5651", 7878787880L));
		accounts.add(new BeneficiaryAccountDto("vikesh", "UBIN5652", 7878787881L));

		BeneficiaryDto beneficiaryDto = new BeneficiaryDto();
		beneficiaryDto.setBeneficiaryAccounts(accounts);
		beneficiaryDto.setCustomer(1L);
		
		ResponseDto responseDto=new ResponseDto();
		responseDto.setHttpStatusCode(201);
		
		ResponseEntity<ResponseDto> result=beneficiaryController.addBeneficiaryDetails(beneficiaryDto);
		assertEquals(HttpStatusCode.valueOf(201), result.getStatusCode());
		
	}

}
