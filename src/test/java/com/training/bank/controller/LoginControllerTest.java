package com.training.bank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.bank.dto.LoginDto;
import com.training.bank.dto.LoginResponseDto;
import com.training.bank.service.impl.LoginServiceImpl;

@ExtendWith(SpringExtension.class)
class LoginControllerTest {

	@InjectMocks
	private LoginController loginController;

	@Mock
	private LoginServiceImpl loginServiceImpl;

	@Test
	void testRegisterCustomer() {
		LoginDto dto = new LoginDto("darshan@gmail.com","sdsd");
//		dto.setEmail("darshan@gmail.com");
//		dto.setPassword("sdsd");

		LoginResponseDto responseDto = new LoginResponseDto();
		responseDto.setUserName("darshan");
		responseDto.setAccountBalance(Double.valueOf(2000));

		Mockito.when(loginServiceImpl.customerLogin(dto)).thenReturn(responseDto);
		ResponseEntity<LoginResponseDto> result = loginController.registerCustomer(dto);

		assertEquals("darshan", result.getBody().getUserName());
		assertEquals(Double.valueOf(2000), result.getBody().getAccountBalance());

	}

}
