package com.training.bank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.bank.dto.FundTransferDto;
import com.training.bank.dto.ResponseDto;
import com.training.bank.entity.TransactionType;
import com.training.bank.service.impl.FundTransferServiceImpl;

@ExtendWith(SpringExtension.class)
class FundTransferControllerTest {

	@InjectMocks
	FundTransferController controller;

	@Mock
	FundTransferServiceImpl fundTransferServiceImpl;

	@Test
	void testFundtransfer() {

		FundTransferDto dto = new FundTransferDto();
		dto.setTransactionAmount(Double.valueOf(500));
		dto.setAccountNumber(12121212121l);
		dto.setBeneficiaryAccountNumber(6356256215372l);
		dto.setTransactionType(TransactionType.DEBIT);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setHttpStatusCode(201);

		ResponseEntity<ResponseDto> result = controller.fundTransfer(dto);
		assertEquals(HttpStatusCode.valueOf(201), result.getStatusCode());
	}

}
