package com.training.bank.controller;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.bank.dto.RegisterDto;
import com.training.bank.dto.ResponseDto;
import com.training.bank.service.impl.RegisterServiceImpl;

@ExtendWith(SpringExtension.class)
 class RegisterControllerTest {
	
	@Mock
	RegisterServiceImpl registerServiceImpl;
	
	@InjectMocks
	RegisterController registerController;
	
	
	@Test
	void testRegistercustomer() {
		RegisterDto dto=new RegisterDto();
		dto.setCustomerName("darshan");
		dto.setEmail("darshan@gmail.com");
		
		ResponseDto responseDto=new ResponseDto();
		responseDto.setHttpStatusCode(201);
		
		ResponseEntity<ResponseDto> result=registerController.registerCustomer(dto);
		assertEquals(HttpStatusCode.valueOf(201), result.getStatusCode());
		
	}

}