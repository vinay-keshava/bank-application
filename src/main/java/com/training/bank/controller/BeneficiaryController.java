package com.training.bank.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.bank.dto.BeneficiaryDto;
import com.training.bank.dto.ResponseDto;
import com.training.bank.service.impl.BeneficiaryServiceImpl;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class BeneficiaryController {

	BeneficiaryServiceImpl beneficiaryServiceImpl;

	@PostMapping("/customer")
	public ResponseEntity<ResponseDto> addBeneficiaryDetails(@RequestBody BeneficiaryDto beneficiaryDto) {
		return new ResponseEntity<>(beneficiaryServiceImpl.addBenefeciary(beneficiaryDto), HttpStatus.CREATED);
	}
}
