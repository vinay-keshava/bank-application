package com.training.bank.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.bank.dto.RegisterDto;
import com.training.bank.dto.ResponseDto;
import com.training.bank.service.impl.RegisterServiceImpl;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "/customers")
@AllArgsConstructor
public class RegisterController {
	
	private RegisterServiceImpl registerServiceImpl;
	
	@PostMapping
	public ResponseEntity<ResponseDto> registerCustomer(@Valid @RequestBody RegisterDto registerDto){
		return new ResponseEntity<>(registerServiceImpl.registerCustomer(registerDto),HttpStatus.CREATED);
		
	}

}
