package com.training.bank.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.bank.dto.FundTransferDto;
import com.training.bank.dto.ResponseDto;
import com.training.bank.service.impl.FundTransferServiceImpl;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "/customer")
@AllArgsConstructor
public class FundTransferController {

	private FundTransferServiceImpl fundTransferServiceImpl;

	@PostMapping
	public ResponseEntity<ResponseDto> fundTransfer(@RequestBody FundTransferDto dto) {
		return new  ResponseEntity<>(fundTransferServiceImpl.fundTransfer(dto), HttpStatus.CREATED);
	}
}
