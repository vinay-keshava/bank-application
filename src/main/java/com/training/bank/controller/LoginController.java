package com.training.bank.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.bank.dto.LoginDto;
import com.training.bank.dto.LoginResponseDto;
import com.training.bank.service.impl.LoginServiceImpl;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class LoginController {

	LoginServiceImpl loginServiceImpl;
	
	@PutMapping("/customers")
	public ResponseEntity<LoginResponseDto> registerCustomer(@Valid @RequestBody LoginDto loginDto){
		return new ResponseEntity<>(loginServiceImpl.customerLogin(loginDto),HttpStatus.OK);
		
	}
	
}
