package com.training.bank.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;


@Entity
@Data
public class Account {

	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private Long accoundId;
	
	@Enumerated(EnumType.STRING)
	private AccountType accountType;
	
	private Long accountNumber;
	
	@Enumerated(EnumType.STRING)
	private IfscCode accountIfscCode;
	private Double balance;
}
