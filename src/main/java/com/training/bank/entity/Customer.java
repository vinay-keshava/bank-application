package com.training.bank.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.Data;

@Data
@Entity
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long customerId;
	
	private String customerName;
	private String email;
	private String password;

	@Enumerated(EnumType.STRING)
	private LoggedInStatus loggedInStatus;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id")
	private Account account;

	private Long aadharNumber;
	
	
}
