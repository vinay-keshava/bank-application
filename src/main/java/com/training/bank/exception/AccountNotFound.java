package com.training.bank.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountNotFound extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final  String message;
	public AccountNotFound(String message) {
		super(message);
		this.message = message;
	}
	
	

}
