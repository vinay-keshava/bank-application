package com.training.bank.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerAlreadyExists extends RuntimeException{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String message;
	public CustomerAlreadyExists(String message) {
		super();
		this.message = message;
	}
	
}
