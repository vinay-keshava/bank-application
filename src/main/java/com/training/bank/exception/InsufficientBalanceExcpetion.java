package com.training.bank.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InsufficientBalanceExcpetion extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String message;
	public InsufficientBalanceExcpetion(String message) {
		super();
		this.message = message;
	}
	
	
}
