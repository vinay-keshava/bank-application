package com.training.bank.exception;

import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.training.bank.dto.ErrorResponse;
import com.training.bank.dto.ResponseDto;


@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{
	
	  @Value("${spring.application.bad_request}")
	    private String badRequest;
	    
	    @Override
	    protected ResponseEntity<Object> handleMethodArgumentNotValid(
				MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
	        List<String> errors = ex.getBindingResult().getAllErrors().stream().map(error -> error.getDefaultMessage()).toList();
	        return new ResponseEntity<>(new ErrorResponse(badRequest, errors), HttpStatus.BAD_REQUEST);
	    }
	
	@ExceptionHandler(CustomerAlreadyExists.class)
	public ResponseEntity<Object> handleCustomerAlreadyExists(CustomerAlreadyExists exception){
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseDto(exception.getMessage(),HttpStatus.CONFLICT.value()));
	}
	
	@ExceptionHandler(AccountMissMatchException.class)
	public ResponseEntity<Object> handleAccountMissMatchException(AccountMissMatchException exception){
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseDto(exception.getMessage(),HttpStatus.CONFLICT.value()));
	}
	@ExceptionHandler(AccountNotFoundException.class)
	public ResponseEntity<Object> handleAccountNotFoundException(AccountNotFoundException exception){
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseDto(exception.getMessage(),HttpStatus.CONFLICT.value()));
	}
	@ExceptionHandler(CustomerAlreadyLoggedIn.class)
	public ResponseEntity<Object> handleCustomerAlreadyLoggedInException(CustomerAlreadyLoggedIn exception){
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseDto(exception.getMessage(),HttpStatus.CONFLICT.value()));
	}
	
	@ExceptionHandler(CustomerNotLoggedIn.class)
	public ResponseEntity<Object> handleCustomerNotLoggedInException(CustomerNotLoggedIn exception){
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseDto(exception.getMessage(),HttpStatus.CONFLICT.value()));
	}
	@ExceptionHandler(CustomerNotFound.class)
	public ResponseEntity<Object> handleCustomerNotException(CustomerNotFound exception){
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseDto(exception.getMessage(),HttpStatus.CONFLICT.value()));
	}
	
	@ExceptionHandler(InsufficientBalanceExcpetion.class)
	public ResponseEntity<Object> handleInsufficientBalanceException(InsufficientBalanceExcpetion exception){
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseDto(exception.getMessage(),HttpStatus.CONFLICT.value()));
	}
	
	@ExceptionHandler(InvalidCredentialExcpetion.class)
	public ResponseEntity<Object> handleInvalidCredentialException(InvalidCredentialExcpetion exception){
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseDto(exception.getMessage(),HttpStatus.CONFLICT.value()));
	}
	
	
	@ExceptionHandler(LoginFailedException.class)
	public ResponseEntity<Object> handleLoginFailedException(LoginFailedException exception){
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseDto(exception.getMessage(),HttpStatus.CONFLICT.value()));
	}
	
	@ExceptionHandler(BalanceNotSufficient.class)
	public ResponseEntity<Object> handleBalanceNotSufficientFoundException(BalanceNotSufficient exception){
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseDto(exception.getMessage(),HttpStatus.CONFLICT.value()));
	}
	
	
}
