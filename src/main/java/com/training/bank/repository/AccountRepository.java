package com.training.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.bank.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long>{

	Account findByAccountNumber(long temAccount);

}
