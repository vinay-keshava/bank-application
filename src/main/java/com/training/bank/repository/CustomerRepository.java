package com.training.bank.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.bank.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{

	Optional<Customer> findByEmail(String email);
	Optional<Customer> findByCustomerId(Long customerId);
	Customer findByEmailAndPassword(String email, String password);

}
