package com.training.bank.service;

import com.training.bank.dto.BeneficiaryDto;
import com.training.bank.dto.ResponseDto;

public interface BeneficiaryService {

	ResponseDto addBenefeciary(BeneficiaryDto beneficiaryDto);

}
