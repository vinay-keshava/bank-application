package com.training.bank.service;

import com.training.bank.dto.FundTransferDto;
import com.training.bank.dto.ResponseDto;

public interface FundTransferService {

	ResponseDto fundTransfer(FundTransferDto dto);

}
