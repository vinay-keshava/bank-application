package com.training.bank.service;

import com.training.bank.dto.LoginDto;
import com.training.bank.dto.LoginResponseDto;

public interface LoginService {

	LoginResponseDto customerLogin(LoginDto loginDto);

}
