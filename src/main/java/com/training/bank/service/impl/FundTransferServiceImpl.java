package com.training.bank.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.training.bank.dto.FundTransferDto;
import com.training.bank.dto.ResponseDto;
import com.training.bank.entity.Account;
import com.training.bank.entity.Beneficiary;
import com.training.bank.entity.Customer;
import com.training.bank.entity.FundTransfer;
import com.training.bank.exception.AccountMissMatchException;
import com.training.bank.exception.AccountNotFound;
import com.training.bank.exception.CustomerNotFound;
import com.training.bank.exception.InsufficientBalanceExcpetion;
import com.training.bank.repository.AccountRepository;
import com.training.bank.repository.BeneficiaryRepository;
import com.training.bank.repository.CustomerRepository;
import com.training.bank.repository.FundTransferRepository;
import com.training.bank.service.FundTransferService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class FundTransferServiceImpl implements FundTransferService {

	private CustomerRepository customerRepository;

	private AccountRepository accountRepository;

	private BeneficiaryRepository beneficiaryRepository;
	
	private FundTransferRepository fundTransferRepository;

	@Override 
	public ResponseDto fundTransfer(FundTransferDto dto) {
		Optional<Customer> customer = customerRepository.findByCustomerId(dto.getCustomerId());
		if (customer.isEmpty()) {
			log.warn("Customer not found");
			throw new CustomerNotFound("customer not found");
		}

		Account accountExist = accountRepository.findByAccountNumber(dto.getAccountNumber());
		if (Objects.isNull(accountExist)) {
			log.warn("Account not found");
			throw new AccountNotFound("account not found");
		}

		if (!Objects.equals(customer.get().getAccount().getAccountNumber(), (accountExist.getAccountNumber()))) {
			log.warn("account mismatched with customer");
			throw new AccountMissMatchException("account missmatched with customer");
		}

		Beneficiary beneficiary = beneficiaryRepository.findByCustomerCustomerId(dto.getCustomerId());
		
		List<Long> accountList= beneficiary.getBeneficiaryAccount().stream().map(t -> t.getAccountNumber())
				.collect(Collectors.toList());
		if(accountList.contains(dto.getBeneficiaryAccountNumber())) {
			
			
			
			switch (dto.getTransactionType()) {
			case DEBIT: 
				if (accountExist.getBalance()< dto.getTransactionAmount()) {
					log.warn("Insufficient Balance");
					throw new InsufficientBalanceExcpetion("Insufficient Balance");
				}
				accountExist.setBalance(accountExist.getBalance() - dto.getTransactionAmount());
				break;
			case CREDIT:
				accountExist.setBalance(accountExist.getBalance() + dto.getTransactionAmount());
				break;
			default:
				throw new IllegalArgumentException("Unexpected value: ");
			}
			
			accountRepository.save(accountExist);
			FundTransfer fundTransfer= new FundTransfer();
			fundTransfer.setAccountNumber(dto.getAccountNumber());
			fundTransfer.setBeneficiaryAccountNumber(dto.getBeneficiaryAccountNumber());
			fundTransfer.setTransactionAmount(dto.getTransactionAmount());
			fundTransfer.setTransactionDate(LocalDateTime.now());
			fundTransfer.setTransactionType(dto.getTransactionType());
			fundTransferRepository.save(fundTransfer);
			log.info("Transaction Successfull");
			return new ResponseDto("Transaction Successfull", 200);
		}
		else
			throw new AccountNotFound("Beneficiary Account Mismatch and not found");
			
	}

}
