package com.training.bank.service.impl;

import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import com.training.bank.dto.RegisterDto;
import com.training.bank.dto.ResponseDto;
import com.training.bank.entity.Account;
import com.training.bank.entity.AccountType;
import com.training.bank.entity.Customer;
import com.training.bank.entity.IfscCode;
import com.training.bank.entity.LoggedInStatus;
import com.training.bank.exception.CustomerAlreadyExists;
import com.training.bank.repository.AccountRepository;
import com.training.bank.repository.CustomerRepository;
import com.training.bank.service.RegisterService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service

@RequiredArgsConstructor
@Slf4j
public class RegisterServiceImpl implements RegisterService {

	private final CustomerRepository customerRepository;
	private final AccountRepository accountRepository;

	@Override
	public ResponseDto registerCustomer(RegisterDto registerDto) {

		Optional<Customer> customerExist = customerRepository.findByEmail(registerDto.getEmail());
		if (customerExist.isPresent()) {
			throw new CustomerAlreadyExists("customer already registerd");
		}

	
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()";
		String pwd = RandomStringUtils.random(6, characters);
		

		Account account = new Account();
		account.setAccountType(AccountType.SAVINGS);
		account.setAccountIfscCode(IfscCode.CNRB2024);
		account.setBalance(registerDto.getAmount());

		long temAccount = (long) (Math.random() * Math.pow(10, 10));
		while (Objects.nonNull(accountRepository.findByAccountNumber(temAccount))) {
			temAccount = (long) (Math.random() * Math.pow(10, 10));
		}

		account.setAccountNumber(temAccount);
		Customer customer=new Customer();
		customer.setCustomerName(registerDto.getCustomerName());
		customer.setEmail(registerDto.getEmail());
		customer.setPassword(pwd);
		customer.setAadharNumber(registerDto.getAadharNumber());
		customer.setAccount(account);
		customer.setLoggedInStatus(LoggedInStatus.LOGGEDOUT);
		customerRepository.save(customer);
		log.info("customer account created successfully");
		return new ResponseDto("customer account created succesfully", 201);

	}

}
