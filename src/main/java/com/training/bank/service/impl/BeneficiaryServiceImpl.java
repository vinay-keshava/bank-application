package com.training.bank.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.training.bank.dto.BeneficiaryAccountDto;
import com.training.bank.dto.BeneficiaryDto;
import com.training.bank.dto.ResponseDto;
import com.training.bank.entity.Beneficiary;
import com.training.bank.entity.BeneficiaryAccount;
import com.training.bank.entity.Customer;
import com.training.bank.entity.LoggedInStatus;
import com.training.bank.exception.CustomerNotFound;
import com.training.bank.exception.CustomerNotLoggedIn;
import com.training.bank.repository.BeneficiaryRepository;
import com.training.bank.repository.CustomerRepository;
import com.training.bank.service.BeneficiaryService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@AllArgsConstructor
public class BeneficiaryServiceImpl implements BeneficiaryService {

	CustomerRepository customerRepository;
	BeneficiaryRepository beneficiaryRepository;

	@Override
	public ResponseDto addBenefeciary(BeneficiaryDto beneficiaryDto) {
		Optional<Customer> customer = customerRepository.findByCustomerId(beneficiaryDto.getCustomer());

		if (Objects.isNull(customer)) {
			log.warn("Customer Not Found with the ID :" + beneficiaryDto.getCustomer());
			throw new CustomerNotFound("Customer Not Found with the ID :" + beneficiaryDto.getCustomer());
		}

		if (customer.get().getLoggedInStatus().equals(LoggedInStatus.LOGGEDOUT)) {
			log.warn("Customer Not logged");
			throw new CustomerNotLoggedIn("Customer Not LoggedIn");
		}

		Beneficiary beneficiary = new Beneficiary();
		beneficiary.setCustomer(customer.get());
		List<BeneficiaryAccount> accounts = new ArrayList<>();
		
		for (BeneficiaryAccountDto account : beneficiaryDto.getBeneficiaryAccounts()) {
			BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
			beneficiaryAccount.setAccountHolderName(account.getAccountHolderName());
			beneficiaryAccount.setAccountNumber(account.getAccountNumber());
			beneficiaryAccount.setIfscCode(account.getIfscCode());
			accounts.add(beneficiaryAccount);
		}

		beneficiary.setBeneficiaryAccount(accounts);
		beneficiaryRepository.save(beneficiary);

		log.info("Beneficiary account details added successfully");
		return new ResponseDto("Beneficiary account details added successfully", 200);

	}
}
