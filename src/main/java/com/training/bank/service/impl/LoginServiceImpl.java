package com.training.bank.service.impl;

import java.util.Objects;

import org.springframework.stereotype.Service;

import com.training.bank.dto.LoginDto;
import com.training.bank.dto.LoginResponseDto;
import com.training.bank.entity.Customer;
import com.training.bank.entity.LoggedInStatus;
import com.training.bank.exception.CustomerAlreadyLoggedIn;
import com.training.bank.exception.CustomerNotFound;
import com.training.bank.exception.LoginFailedException;
import com.training.bank.repository.CustomerRepository;
import com.training.bank.service.LoginService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

	CustomerRepository customerRepository;

	public LoginServiceImpl(CustomerRepository customerRepository) {
		super();
		this.customerRepository = customerRepository;	
	}

	@Override
	public LoginResponseDto customerLogin(LoginDto loginDto) {

		Customer customer = customerRepository.findByEmailAndPassword(loginDto.email(), loginDto.password());
		if (Objects.isNull(customer)) {
			log.warn("Customer Not Found with the email" + loginDto.email());
			throw new CustomerNotFound("Customer Not Found with the email" + loginDto.email());
		}

		if (customer.getLoggedInStatus().equals(LoggedInStatus.LOGGEDIN)) {
			log.warn("Customer already loggedIn");
			throw new CustomerAlreadyLoggedIn("Customer already LoggedIn");
		}

		if (loginDto.password().equals(customer.getPassword())) {
			customer.setLoggedInStatus(LoggedInStatus.LOGGEDIN);
			
			LoginResponseDto dto = new LoginResponseDto();
			dto.setAccountType(customer.getAccount().getAccountType());
			dto.setAccountNumber(customer.getAccount().getAccountNumber());
			dto.setUserName(customer.getCustomerName());
			dto.setAccountBalance(customer.getAccount().getBalance());
			customerRepository.save(customer);

			log.info("Logged In successfully for the customer" + customer.getCustomerName());
			return dto;
		}

		throw new LoginFailedException("Invalid Credentials");
	}

}
