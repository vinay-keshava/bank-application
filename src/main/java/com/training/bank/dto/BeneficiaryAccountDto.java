package com.training.bank.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BeneficiaryAccountDto {

	@NotBlank(message = "Account Holder name mandatory")
	String accountHolderName;
	
	@NotBlank(message = "IFSC code is mandatory")
	String ifscCode;
	
	@NotBlank(message = "Account Number is mandatory")
	Long accountNumber;
}
