package com.training.bank.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class RegisterDto {
	
	@NotBlank(message = "customer name required")
	private String customerName;
	@Email(message = "enter valid email ")
	@NotNull(message = "email required")
	private String email;
	@NotNull(message = "enter the amount")
	private Double amount;
	@NotNull(message = "enter the adhar number")
	private Long aadharNumber;
}
