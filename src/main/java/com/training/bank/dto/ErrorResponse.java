package com.training.bank.dto;

import java.util.List;

import lombok.Data;

@Data
public class ErrorResponse {

	private final String badRequest;
	private final List<String> errors;
}
