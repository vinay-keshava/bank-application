package com.training.bank.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record LoginDto(@NotBlank(message = "email field is mandatory") 
			@Email(message = "Enter valid email") String email,
		@NotBlank(message = "Password is mandatory") String password) {
}

