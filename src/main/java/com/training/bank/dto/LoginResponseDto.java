package com.training.bank.dto;

import com.training.bank.entity.AccountType;

import lombok.Data;

@Data
public class LoginResponseDto {

	String userName;
	AccountType accountType;
	Long accountNumber;
	Double accountBalance;
}
