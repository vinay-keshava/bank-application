package com.training.bank.dto;

import java.util.List;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class BeneficiaryDto {

	@NotBlank(message = "Customer Id is mandatory")
	Long customer;
	
	List<BeneficiaryAccountDto> beneficiaryAccounts;
	
}
