package com.training.bank.dto;

import com.training.bank.entity.TransactionType;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class FundTransferDto {

	@NotNull(message = "enter the from account  number")
	private Long accountNumber;
	@NotNull(message = "enter the the To account number ")
	private Long beneficiaryAccountNumber;
	@NotNull(message = "enter the transaction type")
	private TransactionType transactionType;
	@NotNull(message = "enter the transaction amount")
	private Double transactionAmount;
	@NotNull(message = "enter the customer Id ")
	private Long customerId;
}
